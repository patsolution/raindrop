# RainDrop Limited Shopline Management Manual

## Adding Product

Go to https://shoplineapp.com/
![Main page](1.png)

Click "Log in" on top right hand corner

![Admin Login Page](2.png)

Enter the email and password

Click "ADMIN LOGIN"

You should see the dashboard

![Dashboard](3.png)

On the left hand side, expand "Products &amp; Category"

Click "My Products"

![My Products](4.png)

You should see a list of products

![Existing Products](5.png)

Click "Add" button


![Add Product Photo](6.png)

Click "Add Photo"

![Add Info](7.png)

Click "Info"

Enter name and description of product

![Category](8.png)

Click "Category"

Select the category for product

![Quantity &amp; Pricing](9.png)

Click "Quantity &amp; Pricing"

Enter the cost and price

Click "Add" button

